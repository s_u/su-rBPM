# -*- coding:utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rBPM/version'

Gem::Specification.new do |spec|
  spec.name          = "rBPM"
  spec.version       = RBPM::VERSION
  spec.authors       = ["Tavern", "Overload", "Affettuoso"]
  spec.email         = ["windack@qq.com"]

  spec.summary       = %q{rBPM}
  spec.description   = %q{rBPM}
  # spec.homepage      = "Put your gem's website or public repo URL here."

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features|document)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "Set to 'http://mygemserver.com' to prevent pushes to rubygems.org, or delete to allow pushes to any server."
  end

  spec.add_development_dependency "bundler", "~> 1.9.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "nokogiri"
end
