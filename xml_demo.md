```xml
<?xml version="1.0" encoding="UTF-8" ?>
<definitions expressionLanguage="http://www.w3.org/1999/XPath">
  <process id="myProcess" isExecutable="true" name="My process"
    viewConfig="viewConfig2">
    <startEvent id="startevent1" name="Start"/>
    <sequenceFlow id="flow1" sourceRef="startevent1" targetRef="serviceTask1"/>
    <serviceTask beforeStart="serviceTaskBeforeStart" id="serviceTask1"
      name="serviceTask1"/>
    <sequenceFlow id="flow2" sourceRef="serviceTask1" targetRef="usertask1"/>
    <userTask assignRule="assignScript1" beforeStart="userTaskBeforeStart"
      id="usertask1" name="userTask" viewConfig="viewConfig1"/>
    <sequenceFlow id="flow3" sourceRef="usertask1" targetRef="exclusiveGateway1"/>
    <exclusiveGateway id="exclusiveGateway1" name="exclusiveGateway1"/>
    <sequenceFlow exclusiveFlowTrigger="exclusiveTrue" id="flow4"
      sourceRef="exclusiveGateway1" targetRef="usertask2"/>
    <sequenceFlow exclusiveFlowTrigger="exclusiveFalse" id="flow5"
      sourceRef="exclusiveGateway1" targetRef="endevent1"/>
    <userTask assignRule="assignScript2" id="usertask2" name="usertask2"
      viewConfig="viewConfig2"/>
    <sequenceFlow id="flow6" sourceRef="usertask2" targetRef="parallelGateway1"/>
    <parallelGateway id="parallelGateway1" name="parallelGateway1"/>
    <sequenceFlow id="flow7" sourceRef="parallelGateway1" targetRef="usertask3"/>
    <sequenceFlow id="flow8" sourceRef="parallelGateway1" targetRef="usertask4"/>
    <userTask afterEnd="afterEnd" assignRule="assignScript2" beforeEnd="beforeEnd"
      id="usertask3" name="usertask3" viewConfig="viewConfig3"/>
    <userTask afterEnd="afterEnd" assignRule="assignScript2" beforeEnd="beforeEnd"
      id="usertask4" name="usertask4" viewConfig="viewConfig4"/>
    <sequenceFlow id="flow10" sourceRef="usertask3" targetRef="parallelGateway2"/>
    <sequenceFlow id="flow11" sourceRef="usertask4" targetRef="parallelGateway2"/>
    <parallelGateway id="parallelGateway2" name="parallelGateway2"/>
    <sequenceFlow id="flow13" sourceRef="parallelGateway2" targetRef="endevent1"/>
    <endEvent id="endevent1" name="End"/>
    <extend>
      <script id="serviceTaskBeforeStart">
        p @parameters
      </script>
      <script id="userTaskBeforeStart">
        p 'userTask start!'
      </script>
      <script id="exclusiveTrue">
        true
      </script>
      <script id="exclusiveFalse">
        false
      </script>
      <script id="assignScript1">
        @parameters.car
      </script>
      <script id="assignScript2">
        2
      </script>
      <script id="beforeEnd">
        p "beforeEnd: #{@parameters}"
      </script>
      <script id="afterEnd">
        p "afterEnd: #{@parameters}"
      </script>
      <script id="assginUserTask5">
        @parameters.user_id
      </script>
      <viewConfig id="viewConfig1">
        <acceptParams>name,number</acceptParams>
        <showUrl>/admin/engines/usertask1</showUrl>
        <showConfigs>viewConfig1</showConfigs>
      </viewConfig>
      <viewConfig id="viewConfig2">
        <acceptParams>name,number</acceptParams>
        <showUrl>/admin/engines/usertask2</showUrl>
        <showConfigs>viewConfig2</showConfigs>
      </viewConfig>
      <viewConfig id="viewConfig3">
        <acceptParams>name,number</acceptParams>
        <showUrl>/admin/engines/usertask3</showUrl>
        <showConfigs>viewConfig3</showConfigs>
      </viewConfig>
      <viewConfig id="viewConfig4">
        <acceptParams>name,number</acceptParams>
        <showUrl>/admin/engines/usertask4</showUrl>
        <showConfigs>viewConfig4</showConfigs>
      </viewConfig>
    </extend>
  </process>
</definitions>
```