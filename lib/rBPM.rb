# coding:utf-8
require 'bundler/setup'
require 'rBPM/version'
require 'rails'
require 'active_record'
# require "#{__dir__}/rBPM/load"


module RBPM
  extend ActiveSupport::Autoload

  Dir.glob("#{__dir__}/rBPM/errors/*_error.rb").each { |file|
    require file
  }
  Dir.glob("#{__dir__}/generators/*_generator.rb").each { |file|
    require file
  }
  autoload :Parser, 'rBPM/components/parser_module'
  autoload :Toolkit, 'rBPM/components/toolkit_module'
  autoload :Params, 'rBPM/components/params_module'
  autoload :Execute, 'rBPM/components/execute_module'
  autoload :Engine

  Dir.glob("#{__dir__}/rBPM/bpmn/*.rb").each { |file|
    require file
  }

  Dir.glob("#{__dir__}/rBPM/models/*.rb").each { |file|
    require file
  }

  Dir.glob("#{__dir__}/rBPM/controllers/*.rb").each { |file|
    require file
  }

end
