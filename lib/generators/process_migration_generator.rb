# coding:utf-8
require 'rails/generators'
module RBPM
  module Generators
    class RBPMMigrationGenerator < Rails::Generators::Base

      namespace 'rBPM:migration'
      source_root File.expand_path('../templates', __FILE__)

      desc '给BPM创建迁移文件'

      def copy_migration_files
        time = Time.now.utc.strftime('%Y%m%d%H%M%S')[0...-1]
        copy_file 'migrations/create_process_defines.rb', "db/migrate/#{time}1_create_process_defines.rb"
        copy_file 'migrations/create_process_instances.rb', "db/migrate/#{time}2_create_process_instances.rb"
        copy_file 'migrations/create_process_tasks.rb', "db/migrate/#{time}3_create_process_tasks.rb"
        copy_file 'migrations/create_process_variables.rb', "db/migrate/#{time}4_create_process_variables.rb"
        copy_file 'migrations/create_process_active_instances.rb', "db/migrate/#{time}5_create_process_active_instances.rb"
        copy_file 'migrations/create_process_define_versions.rb', "db/migrate/#{time}6_create_process_define_versions.rb"
      end
    end
  end
end