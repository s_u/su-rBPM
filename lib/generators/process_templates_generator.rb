# coding:utf-8
require 'rails/generators'
module RBPM
  module Generators
    class RBPMTemplatesGenerator < Rails::Generators::Base

      namespace 'rBPM:templates'
      source_root File.expand_path('../templates', __FILE__)

      desc '给BPM创建通用分布模板'

      def copy_templates_files
        copy_file 'templates/_engine_actions.html.erb', 'app/views/layouts/_engine_actions.html.erb'
      end
    end
  end
end