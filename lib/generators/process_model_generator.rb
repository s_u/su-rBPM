# coding:utf-8

require 'rails/generators'
module RBPM
  module Generators
    class RBPMModelGenerator < Rails::Generators::Base

      namespace 'rBPM:model'
      source_root File.expand_path('../templates', __FILE__)

      desc '给BPM创建模型'

      def copy_model_files
        copy_file 'models/process_define.rb', 'app/models/process_define.rb'
        copy_file 'models/process_instance.rb', 'app/models/process_instance.rb'
        copy_file 'models/process_task.rb', 'app/models/process_task.rb'
        copy_file 'models/process_variable.rb', 'app/models/process_variable.rb'
        copy_file 'models/process_active_instance.rb', 'app/models/process_active_instance.rb'
        copy_file 'models/process_define_version.rb', 'app/models/process_define_version.rb'
      end
    end
  end
end