class CreateProcessDefines < ActiveRecord::Migration
  def change
    create_table :process_defines do |t|
      t.string :bpmn_name
      t.integer :current_version_code, default: 0
      t.string :bpmn_id
      t.string :bpmn_key

      t.timestamps
    end
  end
end