class CreateProcessTasks < ActiveRecord::Migration
  def change
    create_table :process_tasks do |t|
      t.belongs_to :process_instance, null: false
      t.belongs_to :process_active_instance, null: false
      t.string :bpmn_name
      t.string :description
      t.integer :assignee_id
      t.integer :priority
      t.string :assignee_type
      t.string :bpmn_define_id
      t.integer :status
      t.string :show_url

      t.datetime :start_at
      t.datetime :end_at
      t.datetime :suspension_at
      t.datetime :due_date
      t.datetime :log_time

      t.timestamps
    end
  end
end