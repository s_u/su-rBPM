class CreateProcessVariables < ActiveRecord::Migration
  def change
    create_table :process_variables do |t|
      t.belongs_to :process_instance, null: false
      t.string :variable_key, null: false
      t.integer :variable_type, default: 0
      t.string :value_string, default: nil
      t.binary :value_file, default: nil
      t.integer :value_number, default: nil
      t.boolean :value_boolean, default: nil
      t.integer :created_task_id
      t.integer :updated_task_id

      t.timestamps
    end
  end
end