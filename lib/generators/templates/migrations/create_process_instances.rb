class CreateProcessInstances < ActiveRecord::Migration
  def change
    create_table :process_instances do |t|
      t.belongs_to :process_define
      t.integer :creator_id
      t.datetime :start_at
      t.datetime :end_at
      t.integer :status
      t.string :business_key
      t.integer :version, default: 0, null: false

      t.timestamps
    end
  end
end