class CreateProcessActiveInstances < ActiveRecord::Migration
  def change
    create_table :process_active_instances do |t|
      t.belongs_to :process_instance, null: false
      t.belongs_to :process_define, null: false
      t.string :bpmn_active_define_id, null: false
      t.integer :current_task_id
      t.string :bpmn_active_name
      t.integer :active_type
      t.integer :assignee
      t.datetime :start_date
      t.datetime :end_date
      t.integer :status
      t.datetime :due_date
      t.integer :duration
      t.integer :executor
      t.string :bpmn_next_target_ref
      t.integer :version, default: 0

      t.timestamps
    end
  end
end