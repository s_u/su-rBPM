class CreateProcessDefineVersions < ActiveRecord::Migration
  def change
    create_table :process_define_versions do |t|
      t.belongs_to :process_define, null: false
      t.string :bpmn_name
      t.integer :version_code, null: false, default: 0
      t.string :bpmn_id
      t.binary :bpmn_file, null: false

      t.timestamps
    end
  end
end