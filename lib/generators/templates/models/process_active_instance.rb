class ProcessActiveInstance < ActiveRecord::Base
  belongs_to :process_instance
  belongs_to :process_define
  has_one :current_task, class_name: 'ProcessTask'
  # has_one :assignee, class_name: ''
  # has_one :executor, class_name: ''

  validates :process_instance_id, :process_define_id, presence: true
end
