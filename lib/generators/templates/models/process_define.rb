class ProcessDefine < ActiveRecord::Base
  has_many :process_define_versions
  has_many :process_instances
  has_many :process_active_instances
  has_many :process_tasks, through: :process_instances
  has_many :process_variables, through: :process_instances

end
