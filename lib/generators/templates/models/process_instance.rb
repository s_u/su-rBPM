class ProcessInstance < ActiveRecord::Base
  belongs_to :process_define
  has_many :process_tasks
  has_many :process_active_instances
  has_many :process_variables
  # has_one :creator, class_name: ''

  validates :process_define_id, presence: true
end
