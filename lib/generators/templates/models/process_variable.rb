class ProcessVariable < ActiveRecord::Base
  belongs_to :process_instance
  belongs_to :process_task
  belongs_to :updated_task, class_name: 'ProcessTask', foreign_key: 'updated_task_id'

  validates :process_instance_id, presence: true
end
