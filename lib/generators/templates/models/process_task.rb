class ProcessTask < ActiveRecord::Base
  belongs_to :process_instance
  belongs_to :process_active_instance
  # has_one :assignee, class_name: ''

  validates :process_instance_id, :process_active_instance_id, presence: true
end
