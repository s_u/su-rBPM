require 'logger'
class RBPM::Log
  class << self

    def general_error(m)
      file = File.open("#{Rails.root}/log/error.log", File::WRONLY | File::APPEND | File::APPEND)
      logger = Logger.new(file, 'daily')
      logger.level = Logger::INFO
      logger.error(m)
      logger.close
    end

    def payment_write(message)
      path = "#{Rails.root}/log/#{date_dir_name}/"
      Dir.mkdir(path) unless Dir.exist? path
      file = File.open("#{path}payment.log", File::WRONLY | File::APPEND | File::CREAT)
      logger = Logger.new(file, 'daily')
      logger.level = Logger::DEBUG
      logger.info(message)
      logger.close
    rescue => e
      general_error e.message
    end


    private

    def date_dir_name
      Time.now.strftime('%Y-%m-%d')
    end
  end
end