require 'nokogiri'

module RBPM
  module Parser
    attr_accessor :doc

    def self.read(str)
      Nokogiri::XML(str)
    end

    def self.node(doc, str)
      doc.at_css(str)
    end

    def self.nodes(doc, str)
      doc.css(str)
    end

    def read(str)
      @doc = RBPM::Parser.read str
    end

    #　获取当前节点以后的所有节点
    def the_next_flows(id)
      flow_nodes=nodes("[sourceRef=#{id}]")
      flows = []
      flow_nodes.each do |node|
        flows << sequence_flow(node)
      end
      flows
    end

    # 选择确定一个节点
    def node(str)
      @doc.at_css(str)
    end

    # 选择节点
    def nodes(str)
      @doc.css(str)
    end

    private

    # 选择器组装
    def selector_assemble(selector_dic={})
      "#{(selector_dic.map { |k, v| "[#{k}=#{v}]" }).join('')}" if selector_dic
    end

    # 选定工作流
    def process_filter(process={})
      "process#{selector_assemble(process)} "
    end
  end
end