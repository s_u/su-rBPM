module RBPM
  module Params
    # 变量初始化
    def process_variables_init(process_instance)
      @parameters||={}
      if @parameters[:process_variables_init]
        @parameters
      else
        process_vars ||= {}
        if process_instance.process_variables.exists?
          process_instance.process_variables.each do |var|
            process_vars[var.variable_key.to_sym] = variables var
          end
        end
        @parameters=@parameters.merge(process_vars).merge({process_variables_init: true})
      end
    end

    def variables(var)
      case var.variable_type
        when 0
          var.value_string
        when 1
          var.value_number
        when 2
          var.value_boolean
        when 3
          var.value_file
        else
          nil
      end
    end
  end
end