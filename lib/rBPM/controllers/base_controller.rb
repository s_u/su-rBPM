module RBPM
  module ControllerTaskModule
    protected

    # 任务是否存在
    def task_present?
      @task.present?
    end

    # 请求的时候，如果有task_id， 则自动获取task实例
    def task
      @task ||= ProcessTask.find_by id: params[:process_task_id] if params[:process_task_id]
      @process_active_instance ||= @task.process_active_instance if @task
      @process_instance ||= @process_active_instance.process_instance if @process_active_instance
      @task
    end

    # 任务提交
    def task_commit
      @process_variables ||={}
      @process_parameters ||= {}
      task.finish!(@process_parameters, @process_variables)
    end

  end
end