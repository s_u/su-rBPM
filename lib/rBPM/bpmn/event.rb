module RBPM
  class Event
    include RBPM::Engine

    def next
      next_flow
    end
  end
end