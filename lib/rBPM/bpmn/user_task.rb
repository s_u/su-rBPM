module RBPM
  class UserTask
    include RBPM::Engine

    attr_reader :active

    def initialize(*args)
      super(*args)

      # 创建活动实例， 如果存在则获取，包含执行beforeStart和afterStart
      @active = create_active_instance
    end

    def next
      next_flow
    end
  end
end