module RBPM
  class ParallelGateway
    include RBPM::Engine
    attr_reader :active

    def initialize(*args)
      super(*args)

      @active = create_active_instance
    end

    def tasks_finished?
      can = true
      flows = nodes("[targetRef=#{id}]")
      task_or_gate_ids= flows.map { |flow| flow[:sourceRef] }

      # 任务复杂性，可能一个活动有循环情况，则按创建时间逆序排序取第一个
      task_or_gate_ids.each do |id|
        active = @process_instance.process_active_instances.where(bpmn_active_define_id: id).order('start_date DESC').first
        can = can && active.status==0 && active.bpmn_next_target_ref == self.id
      end
      can
    end
  end
end