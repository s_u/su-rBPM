module RBPM
  class SequenceFlow
    include RBPM::Engine

    def next_safe
      self.next
    end

    def next
      nodes = nodes("##{targetRef}")
      raise RBPM::DesignError.new('multiple node after flow') if nodes.length!=1
      node = nodes[0]
      flow_node_initial(node)
    end
  end
end