module RBPM
  class ServiceTask
    include RBPM::Engine
    include RBPM::Params

    attr_reader :active

    def initialize(*args)
      super(*args)

      # 创建活动实例， 如果存在则获取，包含执行beforeStart和afterStart
      @active = create_active_instance
    end

    def next(parameters={})
      @parameters ||={}
      @parameters = @parameters.merge(parameters)
      eval(beforeEnd) if defined? beforeEnd
      eval(afterEnd) if defined? afterEnd
      next_flow
    end

  end
end