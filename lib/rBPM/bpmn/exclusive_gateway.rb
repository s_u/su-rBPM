module RBPM
  class ExclusiveGateway
    include RBPM::Engine
    attr_reader :active

    def initialize(*args)
      super(*args)

      @active = create_active_instance
    end

    def which_can_go(parameters={})
      available_flows = []
      @parameters ||={}
      @parameters = @parameters.merge parameters
      the_next_flows(id).each do |flow|
        (available_flows << flow) if (defined? flow.exclusiveFlowTrigger) && eval(flow.exclusiveFlowTrigger)
      end

      raise RBPM::DesignError.new('too many flows or no one runs success!') if available_flows.length!=1

      available_flows.first
    end

    # 进行下一个顺序流，或执行顺序流执行
    def next(which_flow)
      which_flow
      # if which_flow
      #   next_flow_special which_flow.id
      # else
      #   next_flow
      # end
    end

  end
end