class ProcessDefine < ActiveRecord::Base

  def get_version(version)
    self.process_define_versions.find_by(version_code: version)
  end

  def current_version
    self.get_version(self.current_version_code)
  end


  def create_instance(user_id, business_key=nil)
    process = ProcessInstance.create process_define_id: self.id, creator_id: user_id, status: 1, version: self.current_version_code, business_key: business_key
    process.start
    process
  end

  def bpmn_file(version=nil)
    if version.present?
      version_obj = self.get_version(version)
    else
      version_obj = current_version
    end

    current_version.bpmn_file if version_obj
  end
end