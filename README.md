# RBPM


## BPMN协议

本项目是BPMN协议的一个实现，也只实现了一个子集。只包括：startEvent、endEvent、serviceTask、userTask、parallelGateway、exclusiveGateway、sequenceFlow

在此基础上进行了扩展：

扩展属性有：

    beforeStart                 活动开始之前执行，作用于所有Task，值是script标签的id
    afterStart                  活动开始之后执行，作用于所有Task，值是script标签的id
    beforeEnd                   活动结束之前执行，作用于所有Task，值是script标签的id
    afterEnd                    活动结束之后执行，作用于所有Task，值是script标签的id
    exclusiveFlowTrigger        排他路由顺序流判断是否可继续，作用于exclusiveGateway之后的sequenceFlow。
                                用于判断当前sequenceFlow能否继续。值是script标签的id，脚本返回值必须是布尔值
    assignRule                  派任用户脚本，值是script标签的id，返回值是用户的id
    viewConfig                  视图配置标签，作用于所有userTask，值是script的标签的id

扩展标签：

    script                      脚本标签，内部是程序代码。如：<script>User.create! email: 'abc@abc.com' </script>
    viewConfig                  视图配置

如：

```xml
 <viewConfig>
    ...
    <showUrl>/path/to/your/page</showUrl>          必须的标签
    ...
 </viewConfig>
```

具体的协议描述请参考: http://www.mossle.com/docs/jbpm4devguide/html/bpmn2.html

RBPMN文档示例: http://git.oschina.net/tavern/rBPM/blob/master/xml_demo.md



## 安装

将下面这行加入到你程序的Gemfile文件：

```ruby
gem 'rBPM', git: 'https://git.oschina.net/tavern/rBPM.git'
```

添加完之后执行:

    $ bundle

## 使用
### 可用命令

本插件有3个生成命令：

    生成引擎数据库的迁移文件

    $ rails g rBPM:migration

    生成引擎分布视图

    $ rails g rBPM:templates

    生成引擎需要的模型

    $ rails g rBPM:model


### Model中可用方法

#### ProcessDefine

create_instance:method   创建流程实例
    参数      user_id:integer
    返回值     ProcessInstance:object


####ProcessTask

show_url:method  获取用户任务记录的业务地址
    返回值     string

view_configs:Hash   获取BPMN文档中记录的视图配置，固定数据

### Controller中的使用


在需要使用引擎的Controller继承 RBPM::BaseController， 如：

```ruby
class YourController < RBPM::BaseController
```

Controller通过参数`process_task_id`来确认用户任务，以下方法均是在传有该属性时可用：


```ruby
class RBPM::BaseController < ApplicationController::Base
    # 任务是否存在
    def task_present?
      @task.present?
    end

    # 获取当前传入的 process_task_id确定的用户任务
    def task
    end

    # 任务提交
    def task_commit
        # 自动获取流程变量@process_variables        hash类型，此类型中的数据将会持久化
        # 自动获取活动变量@process_parameters       hash类型，一次性使用，一直生效到下一个用户任务
    end
end
```

如：

```ruby

# 此段代码演示了当前任务提交。 此controller中设置了after_action， 当执行了deal_with这个action之后， 自动任务提交。
# deal_with中设置了2个实例变量。 task_commit会自动获取这两个实例变量当做参数

class YourController < RBPM::BaseController

    after_action :task_commit, only: [:task_commit]

    def deal_with
        # your code
        @process_variables = {name: 'Tavern', age: 100}
        @process_parameters = {address: 'CQ'}
    end
end

```


### 视图中得使用

在通过`YourController < RBPM::BaseController`控制器生成出来的页面中，请加上分布视图，此视图是会自动设置当前用户任务的id，提交之时业务代码不需要传用户任务的id

```ruby

class YourController < RBPM::BaseController

    after_action :task_commit, only: [:task_commit]

    def deal_with
        # your code
        @process_variables = {name: 'Tavern', age: 100}
        @process_parameters = {address: 'CQ'}
    end
end

```

以下视图没有使用rails helper，若要使用请替换相应方法。 主要的语句在 `<%= render partial: 'layouts/engine_actions' %>`

```html

<form action="<%= your_deal_with_path %>" method="post">
  <%= render partial: 'layouts/engine_actions' %>
  <button type="submit">提交</button>
</form>

```



# 提交你的代码

1. Fork it ( https://github.com/[my-github-username]/rBPM/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
